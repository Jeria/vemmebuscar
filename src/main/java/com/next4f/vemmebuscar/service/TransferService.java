package com.next4f.vemmebuscar.service;

import com.next4f.vemmebuscar.domain.Transfer;
import com.next4f.vemmebuscar.repository.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransferService {

    @Autowired
    private TransferRepository transferRepository;

    public Transfer save(Transfer transfer) {
        return transferRepository.save(transfer);
    }

}
