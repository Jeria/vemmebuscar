package com.next4f.vemmebuscar.service;

import com.next4f.vemmebuscar.domain.Arrival;
import com.next4f.vemmebuscar.domain.SharedTime;
import com.next4f.vemmebuscar.repository.SharedTimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SharedTimeService {

    @Autowired
    private SharedTimeRepository sharedTimeRepository;

    public List<SharedTime> findAllByArrival(Arrival arrival) {
        return sharedTimeRepository.findAllByArrival(arrival);
    }
}
