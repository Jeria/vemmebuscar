package com.next4f.vemmebuscar.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Data
public class SharedTime {

    @Id
    private Integer id;

    @Column
    private LocalTime hour;

    @ManyToOne
    private Arrival arrival;

}
