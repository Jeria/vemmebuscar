package com.next4f.vemmebuscar.util;

public class EnumUtils {


    public static boolean isPresent(Enum enumArray[], String name) {
        for (Enum element: enumArray ) {
            if(element.name().equals(name))
                return true;
        }
        return false;
    }

    public static <T extends Enum<T>> boolean enumContains(Class<T> enumerator, String value)
    {
        for (T c : enumerator.getEnumConstants()) {
            if (c.name().equals(value)) {
                return true;
            }
        }
        return false;
    }

}
