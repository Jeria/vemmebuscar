package com.next4f.vemmebuscar.util;

import com.next4f.vemmebuscar.domain.OrderStatusEnum;
import com.next4f.vemmebuscar.domain.PurchaseOrder;
import com.next4f.vemmebuscar.domain.Transfer;

import java.time.LocalDate;
import java.time.LocalTime;

public class DomainUtils {

    public static PurchaseOrder createPurchaseOrder(Transfer transfer) {
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setDate(LocalDate.now());
        purchaseOrder.setHour(LocalTime.now());
        purchaseOrder.setOrderStatus(OrderStatusEnum.PENDING);
        int amountOfPeople = transfer.getNumberOfAdults() + transfer.getNumberOfChildren() + transfer.getNumberOfBabies();

        if (/*amountOfPeople > 6 &&*/ transfer.getDestination().getFixedPrice() == null)
            purchaseOrder.setTotalAmount(amountOfPeople * transfer.getDestination().getVariablePrice());
        else
            purchaseOrder.setTotalAmount(transfer.getDestination().getFixedPrice() - ((transfer.getDestination().getPercentageDiscount() / 100) * transfer.getDestination().getFixedPrice()));

        return purchaseOrder;
    }
}
