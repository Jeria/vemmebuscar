package com.next4f.vemmebuscar.repository;

import com.next4f.vemmebuscar.domain.Arrival;
import com.next4f.vemmebuscar.domain.SharedTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SharedTimeRepository extends JpaRepository<SharedTime, Integer> {

    List<SharedTime> findAllByArrival(Arrival arrival);

}
