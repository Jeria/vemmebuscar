UPDATE `vemmebuscar`.`comune` SET `name`='SANTIAGO CENTRO', `is_available_shared`=true WHERE `id`='1';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=true WHERE `id`='7';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=true WHERE `id`='10';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=false WHERE `id`='2';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=false WHERE `id`='3';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=false WHERE `id`='4';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=false WHERE `id`='5';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=false WHERE `id`='6';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=false WHERE `id`='8';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=false WHERE `id`='9';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=true WHERE `id`='11';



INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('394','25', '0', 'ONE_WAY_TRIP', '8000', '1', '7', '2');

INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('395', '25', '0', 'ONE_WAY_TRIP', '8000', '1', '11', '2');
INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('396', '25', '0', 'ONE_WAY_TRIP', '8000', '1', '10', '2');
INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('397', '25', '0', 'ONE_WAY_TRIP', '8000', '1', '1', '2');
INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('398', '25', '0', 'ONE_WAY_TRIP', '9000', '2', '7', '2');
INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('399', '25', '0', 'ONE_WAY_TRIP', '9000', '2', '11', '2');
INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('400', '25', '0', 'ONE_WAY_TRIP', '9000', '2', '10', '2');
INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('401', '25', '0', 'ONE_WAY_TRIP', '9000', '2', '1', '2');

INSERT INTO `vemmebuscar`.`shared_time` (`id`, `hour`, `arrival_id`) VALUES ('1', '14:30:00', '1');
INSERT INTO `vemmebuscar`.`shared_time` (`id`, `hour`, `arrival_id`) VALUES ('2', '2:00:00', '1');

INSERT INTO `vemmebuscar`.`shared_time` (`id`, `hour`, `arrival_id`) VALUES ('3', '05:00:00', '2');
INSERT INTO `vemmebuscar`.`shared_time` (`id`, `hour`, `arrival_id`) VALUES ('4', '06:00:00', '2');
INSERT INTO `vemmebuscar`.`shared_time` (`id`, `hour`, `arrival_id`) VALUES ('5', '09:00:00', '2');
INSERT INTO `vemmebuscar`.`shared_time` (`id`, `hour`, `arrival_id`) VALUES ('6', '10:00:00', '2');
INSERT INTO `vemmebuscar`.`shared_time` (`id`, `hour`, `arrival_id`) VALUES ('7', '22:00:00', '2');
INSERT INTO `vemmebuscar`.`shared_time` (`id`, `hour`, `arrival_id`) VALUES ('8', '23:00:00', '2');

UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=true WHERE `id`='2';


INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('402', '25', '0', 'ONE_WAY_TRIP', '9000', '1', '2', '2');
INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('403', '25', '0', 'ONE_WAY_TRIP', '10000', '2', '2', '2');
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=true WHERE `id`='3';
UPDATE `vemmebuscar`.`comune` SET `is_available_shared`=true WHERE `id`='4';

INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('404', '25', '0', 'ONE_WAY_TRIP', '10000', '1', '3', '2');
INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('405', '25', '0', 'ONE_WAY_TRIP', '10000', '1', '4', '2');

INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('406', '25', '0', 'ONE_WAY_TRIP', '11000', '2', '3', '2');
INSERT INTO `vemmebuscar`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('407', '25', '0', 'ONE_WAY_TRIP', '11000', '2', '4', '2');

INSERT INTO `vemmebuscarqa`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('408', '25', '0', 'ROUND_TRIP', '17000', '1', '7', '2');
INSERT INTO `vemmebuscarqa`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('409', '25', '0', 'ROUND_TRIP', '17000', '1', '11', '2');
INSERT INTO `vemmebuscarqa`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('410', '25', '0', 'ROUND_TRIP', '17000', '1', '10', '2');
INSERT INTO `vemmebuscarqa`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('411', '25', '0', 'ROUND_TRIP', '19000', '1', '2', '2');
INSERT INTO `vemmebuscarqa`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('412', '25', '0', 'ROUND_TRIP', '21000', '1', '3', '2');
INSERT INTO `vemmebuscarqa`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('413', '25', '0', 'ROUND_TRIP', '21000', '1', '4', '2');
INSERT INTO `vemmebuscarqa`.`destination` (`id`, `people_range`, `percentage_discount`, `trip_enum`, `variable_price`, `arrival_id`, `comune_id`, `van_type_id`) VALUES ('414', '25', '0', 'ROUND_TRIP', '17000', '1', '1', '2');